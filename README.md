

import { defaultsDeep, reduce, set, get } from 'lodash';

const serverlessConfig = defaultsDeep({
  provider: {
    name: 'azure',
    runtime: 'nodejs12',
    region: 'West US 2',
    stage: 'dev',
    subscriptionId: '26a5b9ae-7a22-4ad3-8f89-03555ef29b36',
  },

  package: {
    individually: false,
  },

  plugins: ['serverless-dotenv-plugin', 'serverless-azure-functions', 'serverless-webpack'],

  cors: {
    allowCredentials: true,
    allowedOrigins: ['*'],
  },

  custom: {
    webpack: {
      includeModules: {
        nodeModulesRelativeDir: '../',
      },
    },
  },
}, baseConfig);

reduce(
  services,
  (result, service) => {

    // Apis
    set(result, ['apim', 'apis'], [
      ...get(result, ['apim', 'apis'], []),
      {
        name: `${service.name}-api`,
        path: service.name,
        subscriptionRequired: false,
        protocols: [process.env.NODE_ENV === 'development' ? 'http' : 'https'],
        authorization: 'none',
      },
    ]);

    // Backends
    set(result, ['apim', 'backends'], [
      ...get(result, ['apim', 'backends'], []),
      {
        name: `${service.name}-backend`,
        url: `api/${service.name}`,
      },
    ]);

    // Functions
    service.functions.forEach((func) => set(
      result,
      ['functions', func.name],
      {
        handler: `lambdas.${func.name}`,
        apim: {
          api: `${service.name}-api`,
          backend: `${service.name}-backend`,
          operations: [
            {
              method: func.method,
              urlTemplate: `/${func.name}`,
              displayName: func.name,
            },
          ],
        },
      },
    ));

    return result;
  },
  serverlessConfig,
);
